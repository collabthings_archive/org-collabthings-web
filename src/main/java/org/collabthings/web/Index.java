package org.collabthings.web;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Resource
@Controller
@RequestMapping(value = "/")
public class Index extends PageComponent {

	@ResponseBody
	@RequestMapping("/index.html")
	public String indexhtml() throws FileNotFoundException, IOException {
		return getFile("index.html");
	}

	@ResponseBody
	@RequestMapping("/")
	public String index() throws FileNotFoundException, IOException {
		return getFile("index.html");
	}
}