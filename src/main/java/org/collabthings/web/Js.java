package org.collabthings.web;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Resource
@Controller
@RequestMapping(value = "/js")
public class Js extends PageComponent {

	@ResponseBody
	@RequestMapping("/{file:.+}")
	public String jsfile(@PathVariable("file") String file) throws FileNotFoundException, IOException {
		return getFile("js/" + file);
	}

}
