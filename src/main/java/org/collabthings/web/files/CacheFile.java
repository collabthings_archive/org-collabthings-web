package org.collabthings.web.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class CacheFile {

	private final int maxtime;
	private long creationtime = System.currentTimeMillis();
	private String filename;
	//
	private String content;

	public CacheFile(String nfilename, int nmaxtime) {
		this.maxtime = nmaxtime;
		this.filename = nfilename;
	}

	public boolean isOld() {
		return (System.currentTimeMillis() - creationtime) > maxtime;
	}

	public String getContent() throws IOException {
		if (content == null) {
			StringBuilder sb = new StringBuilder();
			appendFile(sb);
			content = sb.toString();
		}
		return content;
	}

	private void appendFile(StringBuilder sb) throws IOException {
		URL path = Thread.currentThread().getContextClassLoader().getResource(filename);
		try (BufferedReader fr = new BufferedReader(new FileReader(path.getPath()))) {
			while (true) {
				String line = fr.readLine();
				if (line == null) {
					break;
				}
				//
				sb.append(line);
				sb.append("\n");
			}
			fr.close();
		} catch (FileNotFoundException | NullPointerException e) {
			sb.append("filenotfound " + e + " " + new File(".").getAbsolutePath() + " path:" + path);
		}
	}

	public void setContent(String ncontent) {
		this.content = ncontent;
	}

}
