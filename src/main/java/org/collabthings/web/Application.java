package org.collabthings.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@Configuration
// @ComponentScan("org.collabthings.web")
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {
	private static Class<Application> applicationClass = Application.class;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass, Index.class, Js.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(applicationClass, args);
	}

}
