package org.collabthings.web;

import java.util.prefs.Preferences;

public class WebSettings {

	private static WebSettings settings;
	private Preferences p = Preferences.userNodeForPackage(getClass());

	public static WebSettings getInstance() {
		if (settings == null) {
			WebSettings.settings = new WebSettings();
		}
		return settings;
	}

	public boolean getBoolean(String string) {
		return "true".equals(getParameter(string));
	}

	public String getParameter(String string) {
		String value = p.get(string, null);
		if (value == null) {
			value = "";
			p.put(string, value);
		}
		return value;
	}

}
